import Adapt, { Style } from "@adpt/core";
import { Service, ServiceProps } from "@adpt/cloud";
import { ServiceDeployment } from "@adpt/cloud/k8s";

export function kubeconfig() {
    // tslint:disable-next-line:no-var-requires
    return require("./kubeconfig.json");
}

/*
 * Kubernetes testing style
 */
export const k8sStyle =
    <Style>
        {Service}
        {Adapt.rule((matchedProps) => {
            const { handle, ...remainingProps } = matchedProps;
            return <ServiceDeployment config={kubeconfig()} {...remainingProps} />;
        })}
    </Style>;

