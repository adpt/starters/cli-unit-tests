# cli-unit-tests starter

Do not use this starter. It is intended solely for use by unit tests for the
[Adapt CLI](https://gitlab.com/unboundedsystems/adapt/tree/master/cli).

